class CreateTrailers < ActiveRecord::Migration
  def change
    create_table :trailers do |t|
      t.string      :address
      t.references  :movie
      t.timestamps
    end
  end
end
