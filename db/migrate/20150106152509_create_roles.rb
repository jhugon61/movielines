class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.references  :movie
      t.references  :actor
      t.string      :part

      t.timestamps
    end
  end
end
