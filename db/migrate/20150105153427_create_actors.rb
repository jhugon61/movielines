class CreateActors < ActiveRecord::Migration
  def change
    create_table :actors do |t|
      t.string :first
      t.string :last
      t.string :gender

      t.timestamps
    end
  end
end
