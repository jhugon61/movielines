class AddYearMpaaMovies < ActiveRecord::Migration
  def change
    add_column  :movies, :year, :integer
    add_column  :movies, :mpaa, :string
  end
end
