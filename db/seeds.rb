# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

casablanca = Movie.create(title:'Casablanca',year:1942,mpaa:"R")
casablanca.trailers << Trailer.create(address:'https://www.youtube.com/watch?v=jQTWfGOv1JY')

Actor.create(first:'Ben',last:'Stiller',gender:"M")
Actor.create(first:'Owen',last:'Wilson',gender:"M")
Actor.create(first:'Humphrey',last:'Bogart',gender:"M")

jlh = User.find_by_email('jhugon@andover.edu')
unless jlh
  jlh = User.new(first:'Jacques',last:'Hugon',email:'jhugon@andover.edu',
  password:'andover',password_confirmation:'andover')
  #jlh.skip_confirmation!
  jlh.roles = %w[superuser admin member]
  jlh.save
end
