module ActorsHelper
  
  def show_fullname(actor)
  	  link_to "#{actor.fullname}", actor_path(actor)
	end
  
end
