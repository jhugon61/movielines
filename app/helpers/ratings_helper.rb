module RatingsHelper
  
  def show_current_user_rating(rateable)
    if user_signed_in?
      if rateable.is_a? Movie
        rating = current_user.ratings.for_movies.with_id(rateable).first
      elsif ratable.is_a? Actor
        rating = current_user.ratings.for_actors.with_id(rateable).first
      end
    else
      rating = nil
    end
    if rating.nil?
      disp = content_tag(:span, 'none')
    else
      disp = content_tag(:span, rating.score)
      if can? :update, rating
        disp += content_tag(:a, 'change',
          href:edit_rating_path(rating), class:'btn btn-xs btn-default')
      end
    end
  end
  
end
