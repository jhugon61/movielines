module ApplicationHelper
  
  def show_role(user)
    if user.is?(:superuser)
      content_tag(:span, 'superuser', class:'label label-danger')
    elsif user.is?(:admin)
      content_tag(:span, 'admin', class:'label label-warning')
    elsif user.is(:member)
      content_tag(:span, 'member', class:'label label-success')
    end
  end
  
end
