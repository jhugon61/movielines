module MoviesHelper
  
  def show_mpaa(movie)
    content_tag(:div, movie.mpaa, class:'label label-mpaa')
  end
  
end
