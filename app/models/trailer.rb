# == Schema Information
#
# Table name: trailers
#
#  id         :integer          not null, primary key
#  address    :string(255)
#  movie_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class Trailer < ActiveRecord::Base
  
  belongs_to  :movie      # trailer.movie
  
  validates   :address, presence:true
  #validates   :movie_id, presence:true
  
end
