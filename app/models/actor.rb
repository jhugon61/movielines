# == Schema Information
#
# Table name: actors
#
#  id         :integer          not null, primary key
#  first      :string(255)
#  last       :string(255)
#  gender     :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Actor < ActiveRecord::Base
  
  validates :last, presence:true
  validates :gender, :inclusion=>{:in=>%w(M F)}
  
  #has_and_belongs_to_many :movies
  has_many  :roles, dependent: :destroy
  has_many  :movies, :through=>:roles
  
  has_many  :ratings, as: :rateable, dependent: :destroy
  
  scope :males, -> { where(gender:'M') }
  scope :females, -> { where(gender:'F') }
  
  def fullname
    self.first + " " + self.last
  end
  
end
