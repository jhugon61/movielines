# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  movie_id   :integer
#  actor_id   :integer
#  part       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Role < ActiveRecord::Base
  
  #validates   :movie_id, presence:true
  #validates   :actor_id, presence:true
  
  belongs_to  :movie      # role.movie
  belongs_to  :actor      # role.actor
  
end
