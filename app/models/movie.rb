# == Schema Information
#
# Table name: movies
#
#  id                  :integer          not null, primary key
#  title               :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  year                :integer
#  mpaa                :string(255)
#  poster_file_name    :string(255)
#  poster_content_type :string(255)
#  poster_file_size    :integer
#  poster_updated_at   :datetime
#

class Movie < ActiveRecord::Base
  
  has_many  :trailers, dependent: :destroy     # movie.trailers
  accepts_nested_attributes_for :trailers, :reject_if=>:all_blank, :allow_destroy=>true
  
  #has_and_belongs_to_many :actors
  has_many  :roles, dependent: :destroy
  accepts_nested_attributes_for :roles, :reject_if=>:all_blank, :allow_destroy=>true
  has_many  :actors, through: :roles
  
  has_many  :ratings, as: :rateable, dependent: :destroy
  
  # Paperclip association
  has_attached_file :poster,
    :styles=>{:medium=>"400x400>", :thumb=>"50x50>"},
    :default_url=>"/images/:style/missing.png"
  validates_attachment_content_type :poster, :content_type=>/\Aimage\/.*\z/
  
  validates :title, presence:true, uniqueness:true
  validates :year, presence:true
  validates :mpaa, presence:true, inclusion:{in:MPAA_RATINGS.values}
  
  scope :from_year, ->(year) { where(year:year) }
  scope :after_year, ->(year) { where("year > ?", year) }
  scope :with_rating, ->(rating) { where(mpaa:rating) }
  
end
