# == Schema Information
#
# Table name: ratings
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  rateable_id   :integer
#  rateable_type :string(255)
#  score         :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class Rating < ActiveRecord::Base
  
  belongs_to    :user
  belongs_to    :rateable, polymorphic:true
  
  validates     :score, :inclusion=>0..5
  
  # Rating.for_movies.with_id(6)
  scope         :for_movies, -> { where(rateable_type:'Movie') }
  scope         :for_actors, -> { where(rateable_type:'Actor') }
  scope         :with_id, ->(o) { where(rateable_id:o.id) }
  
end
