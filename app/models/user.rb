# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first                  :string(255)
#  last                   :string(255)
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  roles_mask             :integer          default(0)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise  :database_authenticatable,
          #:registerable,
          #:recoverable,
          :rememberable#,
          #:trackable,
          #:validatable
          
  has_many    :ratings
          
  ROLES = %w[superuser admin member]
  
  def roles=(roles)
    # => %w[admin]
    self.roles_mask = (roles&ROLES).map{|r| 2**ROLES.index(r)}.inject(0,:+)
  end
  
  def roles
    ROLES.reject do |r|
      (roles_mask & 2**ROLES.index(r)).zero?
    end
  end
  
  def is?(role)
    # => true if matches a role for this user, false otherwise
    roles.include?(role.to_s)
  end
  
end
