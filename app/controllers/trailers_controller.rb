class TrailersController < ApplicationController
  
  load_and_authorize_resource
  
  def index
    @trailers = Trailer.all
  end
  
  def show
    @trailer = Trailer.find(params[:id])
  end
  
  def new
    @trailer = Trailer.new
  end
  
  def create
    @trailer = Trailer.new(trailer_params)
    if @trailer.save
      redirect_to @trailer, notice:'trailer successfully created'
    else
      render 'new'
    end
  end
  
  def edit
    @trailer = Trailer.find(params[:id])
  end
  
  def update
    @trailer = Trailer.find(params[:id])
    if @trailer.update(trailer_params)
      redirect_to @trailer, notice: 'trailer successfully updated'
    else
      render 'edit'
    end
  end
  
  def destroy
    @trailer = Trailer.find(params[:id])
    movie = @trailer.movie
    @trailer.destroy unless @trailer.nil?
    redirect_to movie
  end
  
  private
  
  def trailer_params
    params[:trailer].permit(:address, :movie_id)
  end
  
end
