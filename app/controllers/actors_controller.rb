class ActorsController < ApplicationController
  
  # before_action :authenticate_user!, except:[:index,:show]
  
  load_and_authorize_resource

  def new
    @actor = Actor.new
  end
  
  def create
    @actor = Actor.new(actor_params)
    if @actor.save
      redirect_to @actor, notice: 'actor successfully created'
    else
      render :new
    end
  end
  
  def edit
    @actor = Actor.find(params[:id])
  end
  
  def update
    @actor = Actor.find(params[:id])
    if @actor.update_attributes(actor_params)
      redirect_to @actor, notice: 'actor successfully updated'
    else
      render :edit
    end
  end
  
  def index
    @actors = Actor.order(:last, :first)
  end
  
  def show
    @actor = Actor.find(params[:id])
  end
  
  def destroy
    @actor = Actor.find(params[:id])
    @actor.destroy
    redirect_to actors_path, notice: 'actor successfully destroyed'
  end
  
  private
  
  def actor_params
    params[:actor].permit(:first, :last, :gender, {:movie_ids=>[]})
  end
  
end
