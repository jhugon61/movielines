class RolesController < ApplicationController
  
  # before_action :authenticate_user!, except:[:index,:show]
  load_and_authorize_resource
  

  def new
    @movie = Movie.find(params[:movie_id])
    @role = @movie.roles.new
    # @role = Role.new(movie_id:@movie.id)
  end
  
  def create
    @role = Role.new(role_params)
    if @role.save
      redirect_to @role.movie, notice:'role has been created'
    else
      render :new
    end
  end
  
  private
  
  def role_params
    params[:role].permit(:part, :movie_id, :actor_id)
  end
  
end
