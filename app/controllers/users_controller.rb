class UsersController < ApplicationController
  
  def index
    @users = User.all.order(:last, :first)
  end
  
end
