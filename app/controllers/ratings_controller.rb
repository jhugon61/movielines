class RatingsController < ApplicationController
  
  load_and_authorize_resource
  
  def new
    @rating = current_user.ratings.new
    # => params[:movie_id] or params[:actor_id]
    @rating.rateable = Movie.find(params[:movie_id]) if params[:movie_id]
    @rating.rateable = Actor.find(params[:actor_id]) if params[:actor_id]
  end
  
  def create
    @rating = current_user.ratings.new(rating_params)
    if @rating.save
      redirect_to @rating.rateable, notice:'rating created'
    else
      render :new
    end
  end
  
  private
  
  def rating_params
    params[:rating].permit(:score, :user_id, :rateable_id, :rateable_type)
  end
  
end
