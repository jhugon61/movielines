class Mailrobot < ActionMailer::Base
  default from: "andover1778@gmail.com"
  default reply_to: "jhugon@andover.edu"
  
  def notify_movie(user, movie)
    @movie = movie
    @user = user
    attachments[@movie.poster_file_name] =
      File.read(@movie.poster.path) if @movie.poster?
    if Rails.env.development?
      recipient = "jhugon@andover.edu"
    else
      recipient = @user.email
    end
    mail(to:recipient, subject:"#{@movie.title} has just been created!", date:Time.now)
  end
  
end
