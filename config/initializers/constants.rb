MPAA_RATINGS = {
  "(G) General Audiences" => "G",
  "(PG) Parental Guidance Suggested" => "PG",
  "(PG-13) Parents Strongly Cautioned" => "PG-13",
  "(R) Restricted" => "R",
  "(NC-17) Adults Only" => "NC-17"
}
